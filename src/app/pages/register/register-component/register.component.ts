import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/models/interface-user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  	// Incialización del formulario
    public userRegisterForm: FormGroup;
    public submitted: boolean = false;

  constructor(private formBuilder: FormBuilder) {
    this.userRegisterForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.maxLength(20)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });   
   }

  ngOnInit(): void {/* Empty */}
  
  public onSubmit(): void {
    // El usuario ha pulsado en submit->cambia a true submitted
    this.submitted = true;
    // Si el formulario es valido
    if (this.userRegisterForm.valid) {
      // Creamos un Usuario y lo emitimos
      const user: User = {
        email: this.userRegisterForm.get('email')?.value,
        password: this.userRegisterForm.get('password')?.value,
      };
      console.log(user);
      // Reseteamos todos los campos y el indicador de envío o submitted
      this.userRegisterForm.reset();
      this.submitted = false;
    }
  }
}
